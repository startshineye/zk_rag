import json
import requests

from home.models import 对话记录

api_key = '3ad83741a23a52b99dd326679e367a6e.aQZKIGJF3M3NHObL'
url = 'https://open.bigmodel.cn/api/paas/v4/chat/completions'


def chat(messages):
    json_obj = {
        "messages": messages
    }
    playload = json.dumps(json_obj)
    headers = {
        'Authorization': 'Bearer ' + api_key,
        'Content-Type': 'application/json'
    }
    response = requests.request("POST", url, headers=headers, data=playload)
    print(response)
    json_result = json.loads(response.text)
    if "error_code" in json_result:
        return json_result["error_msg"] + "：" + playload
    else:
        result = handle_ai_result(json_result["result"])
    return result


def build_chat_message(previous_user_input, user_input):
    if previous_user_input is not None and len(previous_user_input.strip())>0:
        # 引入之前的输入(之前的输入 从数据库查询)，有一个上下文
        user_input = previous_user_input + user_input

    messages = [
        {
            'role': 'user',
            'content': f"""
              请根据用户的输入返回json格式结果，除此之外不要返回其他内容。注意，模块部分请按以下选项返回对应序号：
               1. 销售对账   
               2. 报价单   
               3. 销售订单   
               4. 送货单   
               5. 退货单   
               6. 其他
               
             示例1：  
             用户：客户北京极客邦有限公司的款项到账了多少？  
             系统：  {{'模块':1,'客户名称':'北京极客邦有限公司'}}
             
             示例2：  
             用户：你好  
             系统：  {{'模块':6,'其他数据',None}}  
             
             示例3：  
             用户：最近一年你过得如何？  
             系统：  {{'模块':6,'其他数据',None}}
            
              用户：{input_msg}  
              系统：  
            """
        }
    ]
    return messages


def handle_ai_result(ai_result):
    result = ai_result.replace("```json", '').replace("```", '')  # 去掉json格式之外无关的内容  return 处理后结果
    return result


def get_structured_data_query_parameters(user_input):
    # 获取之前的数据
    previous_user_input = get_previous_user_input()
    total_retries = 2
    current_retry_count = 0
    while current_retry_count <= total_retries:
        try:
            structured_data = chat(build_chat_message(previous_user_input, user_input))
            query_parameters = json.loads(structured_data)
            return query_parameters
        except:
            current_retry_count += 1

    return None


def get_previous_user_input():
    previous_user_input = ""
    之前的messages = 对话记录.objects.filter(已结束=False).order_by('created_time')
    for current in 之前的messages:
        if current.role == 'user' and current.content is not None:
            previous_user_input += current.content
    return previous_user_input
